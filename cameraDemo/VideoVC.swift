//
//  VideoVC.swift
//  cameraDemo
//
//  Created by praveen velanati on 4/11/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class VideoVC: AVPlayerViewController {
    
    
    var videoURL : NSURL? = nil
    var videoPlayer : AVPlayer? = AVPlayer()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.navigationBar.hidden = false
        loadVideo()
    }
    
    func playerDidReachEndNotificationHandler(notification : NSNotification) {
        
        
        print("playerDidReachEndNotification")
    }
    
    
        
        func loadVideo()
        {
           let videoPlayer = AVPlayer(URL: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = videoPlayer
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "playerDidReachEndNotificationHandler:", name: AVPlayerItemDidPlayToEndTimeNotification, object: videoPlayer.currentItem)
            
            self.presentViewController(playerViewController, animated: true)
            {
                playerViewController.player!.play()
            }
        }
        
        
        
        
        
    
    
    
    
    
    
    

}
