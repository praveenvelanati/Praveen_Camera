//
//  ImageViewController.swift
//  cameraDemo
//
//  Created by praveen velanati on 4/11/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {
    
    
    var image : UIImage?
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     self.navigationController?.navigationBar.hidden = false
       
        if let validImage = image {
        
        self.imageView.image = validImage
        
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    
    
    
}
