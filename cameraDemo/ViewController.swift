//
//  ViewController.swift
//  cameraDemo
//
//  Created by praveen velanati on 4/11/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import UIKit
import CameraManager

class ViewController: UIViewController {

    
    let cameraManager = CameraManager()
  
    @IBOutlet weak var cameraView: UIView!
    
    @IBOutlet weak var recordBtn: UIButton!
    //@IBOutlet weak var modeSelect: UISegmentedControl!
    
    @IBOutlet weak var outputModeBTN: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cameraManager.showAccessPermissionPopupAutomatically = true
        cameraManager.cameraOutputMode = .StillImage
             }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.hidden = true
      
        cameraManager.addPreviewLayerToView(cameraView)
       
        

        cameraManager.resumeCaptureSession()
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        cameraManager.stopCaptureSession()
    }
    
    
    @IBAction func changeFlashMode(sender: UIButton) {
        
        switch(cameraManager.changeFlashMode()){
            
        case .Off:
            sender.setTitle("Flash Off", forState: UIControlState.Normal)
        
        case .On:
            sender.setTitle("Flash On", forState: UIControlState.Normal)
       
        case .Auto:
            sender.setTitle("Flash Auto", forState: UIControlState.Normal)
            
            
            
        }
        
        
    }
    
    
    
    @IBAction func switchCamera(sender: UIButton) {
        
        cameraManager.cameraDevice = cameraManager.cameraDevice == CameraDevice.Back ? CameraDevice.Front : CameraDevice.Back
        
        }
    
    
    
    
    
    
    @IBAction func outputMode(sender: UIButton) {
        
        
        cameraManager.cameraOutputMode = cameraManager.cameraOutputMode == CameraOutputMode.VideoWithMic ? CameraOutputMode.StillImage : CameraOutputMode.VideoWithMic
        
        
        switch (cameraManager.cameraOutputMode) {
        case .StillImage:
            
            recordBtn.selected = false
            recordBtn.backgroundColor = UIColor.greenColor()
            sender.setTitle("Image", forState: UIControlState.Normal)
            
        case .VideoWithMic,.VideoOnly:
            sender.setTitle("Video", forState: UIControlState.Normal)
        }
        
    }
    
    
    
    
    
    
    
   /* @IBAction func switchMode(sender: UISegmentedControl) {
        
        
        if(modeSelect == 0){
            
            cameraManager.cameraOutputMode = CameraOutputMode.VideoWithMic
            
            print("selvid")

            
        } else {
            
            cameraManager.cameraOutputMode = CameraOutputMode.StillImage
            recordBtn.backgroundColor = UIColor.greenColor()

            print("selImg")
        }
        
        
        }*/
    

    
    @IBAction func recordOrCapture(sender: UIButton) {
        
        switch(cameraManager.cameraOutputMode) {
        case .StillImage:
            print("stillmode")
            cameraManager.capturePictureWithCompletition({ (image, error) in
                
    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ImageVC") as? ImageViewController
                
                if let validVC = vc {
                    
                    if let capturedImage = image {
                        
                        validVC.image = capturedImage
                        
       self.navigationController?.pushViewController(validVC, animated: true)
                        
                    }
                    
                    
                    
                }
                
                
                
            })
       
        case .VideoWithMic,.VideoOnly:
            print("videomode")
            sender.selected = !sender.selected
            sender.setTitle("", forState: UIControlState.Selected)
            sender.backgroundColor = sender.selected ? UIColor.redColor() : UIColor.greenColor()
            if sender.selected {
                
                
                cameraManager.startRecordingVideo()
            
            } else {
                cameraManager.stopRecordingVideo({ (videoURL, error) in
                
                   let videoPlayerViewController = self.storyboard!.instantiateViewControllerWithIdentifier("Video") as! VideoVC
                    
                videoPlayerViewController.videoURL = videoURL
                    
                    self.navigationController!.pushViewController(videoPlayerViewController, animated: true)
                    
                    if let errorOccured = error {
                        
                        self.cameraManager.showErrorBlock(erTitle: "Error occured", erMessage: errorOccured.localizedDescription)
                        
                    }
                    
                    
                    
                    
                })
                
                
                
            }
            
            
            
            
        }
        
        
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
   

}

